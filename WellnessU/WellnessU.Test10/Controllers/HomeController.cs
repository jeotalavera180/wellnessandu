﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WellnessU.Entity;
using WellnessU.Repository;

namespace WellnessU.Test10.Controllers
{
    public class HomeController : Controller
    {
        IRepository _repo = null;

        public HomeController()
        {
            _repo = new WellnessRepository();
        }
        public ActionResult Index()
        {
            List<t_User> users = _repo.GetUsers();
            t_Content cont = new t_Content
            {
                id = Guid.NewGuid(),
                user_id = Guid.Parse("f03f4b9c-cd4c-4217-940d-8b1888995476"),
                //t_User = users[0],
                description = "this is a great content from",
                title = "ONLY FOR 5476 ONLY 2",
                tag = "boom,boss,something,somethingelse",
            };

            var result = _repo.CreateContent(cont);
            
            var cin = _repo.SearchContentsPerUser(users[0]);
            var cin2 = _repo.SearchContentsPerUser(users[1]);
            //var contents = _repo.SearchContentsViaTags(new string[] { "s","boompac" });

            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
