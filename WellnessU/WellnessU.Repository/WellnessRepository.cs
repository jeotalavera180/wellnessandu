﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WellnessU.Data;
using WellnessU.Entity;

namespace WellnessU.Repository
{
    public class WellnessRepository : IRepository
    {
        WellnessU_dbEntities _dbContext = null;
        bool enableProxyCreation = false;
        public WellnessRepository()
        {
            this._dbContext = new WellnessU_dbEntities();
            _dbContext.Configuration.ProxyCreationEnabled = enableProxyCreation;
        }

        public WellnessRepository(WellnessU_dbEntities _dbContext)
        {
            this._dbContext = _dbContext;
            _dbContext.Configuration.ProxyCreationEnabled = enableProxyCreation;
        }

        public List<t_User> GetUsers()
        {
            var users = _dbContext.t_User.ToList();
            return users;
        }

        /// <summary>
        /// Search content via tag
        /// </summary>
        /// <param name="tags"></param>
        /// <param name="includeDeleted">if true, also returns deleted content</param>
        /// <returns></returns>
        public List<t_Content> SearchContentsViaTags(string[] tags, bool includeDeleted = false)
        {
            // _dbContext.Configuration.ProxyCreationEnabled = false;
            //c.Status != null ? (includeDeleted ? true : (c.Status == (int)Status.Deleted ? false : true)) : true
            List<t_Content> contentsByTag;
            if (includeDeleted)
            {
                contentsByTag = _dbContext.t_Content
                                .Where(c =>
                                    c.t_Tag.Where(t => tags.Contains(t.name)).Count() > 0)
                                    .ToList();
            }
            else
            {
                contentsByTag = _dbContext.t_Content
                               .Where(c =>
                                   c.t_Tag.Where(t => tags.Contains(t.name)).Count() > 0
                                   &&
                                   c.isDeleted == false)
                                   .ToList();
            }
            return contentsByTag;
        }

        /// <summary>
        /// search content using a criteria name
        /// </summary>
        /// <param name="criteriaName">criteria name</param>
        /// <param name="includeDeleted">if true, also returns deleted content</param>
        /// <returns></returns>
        public List<t_Content> SearchContentsViaCriteria(string criteriaName, bool includeDeleted = false)
        {
            string cName = criteriaName.ToLower().Trim();
            List<t_Content> contentsByCriteria;
            if (includeDeleted)
            {
                contentsByCriteria = _dbContext.t_Content
                .Where(c =>
                    c.t_Criteria.Where(t => cName.ToLower().Trim().Equals(t.name.ToLower().Trim())).Count() > 0
                    )
                    .ToList();
            }
            else
            {
                contentsByCriteria = _dbContext.t_Content
                .Where(c =>
                    c.t_Criteria.Where(t => cName.ToLower().Trim().Equals(t.name.ToLower().Trim())).Count() > 0
                   &&
                   c.isDeleted == false)
                   .ToList();
            }
            return contentsByCriteria;
        }

        /// <summary>
        /// Returns content per user
        /// </summary>
        /// <param name="guid">the id of the user</param>
        /// <param name="includeDeleted">if true, also returns deleted content</param>
        /// <returns></returns>
        public List<t_Content> SearchContentsPerUser(string guid, bool includeDeleted = false)
        {
            List<t_Content> contentsByUser;
            if (includeDeleted)
            {
                contentsByUser = _dbContext.t_Content
                                .Where(c =>
                                    c.t_User.id == Guid.Parse(guid)
                                    )
                                    .ToList();
            }
            else
            {
                contentsByUser = _dbContext.t_Content
                                  .Where(c =>
                                      c.t_User.id == Guid.Parse(guid)
                                      &&
                                      c.isDeleted == false
                                      )
                                      .ToList();
            }
            return contentsByUser;
        }

        /// <summary>
        /// Returns content per user
        /// </summary>
        /// <param name="guid">guid</param>
        /// <param name="includeDeleted">if true, also returns deleted content</param>
        /// <returns></returns>
        public List<t_Content> SearchContentsPerUser(Guid guid, bool includeDeleted = false)
        {
            List<t_Content> contentsByUser;
            if (includeDeleted)
            {
                contentsByUser = _dbContext.t_Content
                                .Where(c =>
                                    c.t_User.id == guid
                                    )
                                    .ToList();
            }
            else
            {
                contentsByUser = _dbContext.t_Content
                                  .Where(c =>
                                      c.t_User.id == guid
                                      &&
                                      c.isDeleted == false
                                      )
                                      .ToList();
            }
            return contentsByUser;
        }

        /// <summary>
        /// Returns content per user
        /// </summary>
        /// <param name="guid">guid</param>
        /// <param name="includeDeleted">if true, also returns deleted content</param>
        /// <returns></returns>
        public List<t_Content> SearchContentsPerUser(t_User user, bool includeDeleted = false)
        {
            List<t_Content> contentsByUser;
            if (includeDeleted)
            {
                contentsByUser = _dbContext.t_Content
                                .Where(c =>
                                    c.t_User.id == user.id
                                    )
                                    .ToList();
            }
            else
            {
                contentsByUser = _dbContext.t_Content
                              .Where(c =>
                                  c.t_User.id == user.id
                                  &&
                                  c.isDeleted == false
                                  )
                                  .ToList();
            }
            return contentsByUser;
        }

        #region unfinished
        public List<t_Content> SearchContentsViaDepartment(string name, bool includeDeleted = false)
        {
            var contents = _dbContext.t_Content.ToList();
            return contents;
        }

        public List<t_Content> SearchContentsViaDepartment(t_Department dep, bool includeDeleted = false)
        {
            var contents = _dbContext.t_Content.ToList();
            return contents;
        }
        //public IQueryable<T> All<T>() where T : class
        //    return _dbContext.Set<T>();
        //{
        //}
        #endregion

        /// <summary>
        /// Create the content
        /// </summary>
        /// <param name="content">the content to be created</param>
        /// <returns></returns>
        public bool CreateContent(t_Content content)
        {
            _dbContext.t_Content.Add(content);
            if (_dbContext.SaveChanges() > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Modify everything about content
        /// </summary>
        /// <param name="content"></param>
        /// <returns>true if the modification is successful</returns>
        public bool SaveChangesContent(t_Content updatedContent)
        {
            var contentInDB = _dbContext.t_Content.Find(updatedContent.id);
            if (contentInDB != null)
            {
                contentInDB.t_Criteria = updatedContent.t_Criteria;
                contentInDB.user_id = updatedContent.user_id;
                contentInDB.t_User = updatedContent.t_User;
                contentInDB.title = updatedContent.title;
                contentInDB.description = updatedContent.description;
                contentInDB.tag = updatedContent.tag;
                contentInDB.Status = updatedContent.isDeleted?(int)Status.Deleted:(int)Status.Updated;

                if (_dbContext.SaveChanges() > 0)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// remove content - not really remove, just mark it as deleted
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public bool RemoveContent(t_Content content)
        {
            content.isDeleted = true;
            return SaveChangesContent(content);
        }

        /// <summary>
        /// remove content - not really remove, just mark it as deleted
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public bool RemoveContent(Guid guid)
        {
            var content = _dbContext.t_Content.Where(c => c.id == guid).Single();
            content.isDeleted = true;
            return SaveChangesContent(content);
        }




        public void CreateTestUpload()
        {
            t_Upload up = new t_Upload
            {
                id = Guid.NewGuid(),
                content_id = Guid.Parse("878c8349-4060-4864-8ed8-dee2778039a4"),
                name = "test",
                Status = (int)Status.Created
            };

            _dbContext.t_Upload.Add(up);

            _dbContext.SaveChanges();
        }


        public void Dispose()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }


        public void AddTagsToContent(string[] tag)
        {
            throw new NotImplementedException();
        }

        public void EditTagToContent(string oldTag, string newTag)
        {
            throw new NotImplementedException();
        }

        public void RemoveTagsToContent(string[] tag)
        {
            throw new NotImplementedException();
        }

        public void AddCriteriaToContent(string[] tag)
        {
            throw new NotImplementedException();
        }

        public void EditCriteriaToContent(string oldTag, string newTag)
        {
            throw new NotImplementedException();
        }

        public void RemoveCriteriaToContent(string[] tag)
        {
            throw new NotImplementedException();
        }







    }
}
