﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WellnessU.Data;
using WellnessU.Entity;

namespace WellnessU.Repository
{
    public interface IRepository:IDisposable
    {
        List<t_User> GetUsers();

        /**
         * Content Operations
         * */
        //search
        List<t_Content> SearchContentsViaTags(string[] tag, bool includeDeleted = false);
        List<t_Content> SearchContentsViaCriteria(string criteria, bool includeDeleted = false);
        List<t_Content> SearchContentsPerUser(string guid, bool includeDeleted = false);
        List<t_Content> SearchContentsPerUser(Guid guid, bool includeDeleted = false);
        List<t_Content> SearchContentsPerUser(t_User user, bool includeDeleted = false);
        List<t_Content> SearchContentsViaDepartment(string name, bool includeDeleted = false);
        List<t_Content> SearchContentsViaDepartment(t_Department dep, bool includeDeleted = false);
        //create content
        bool CreateContent(t_Content content);
        //modify content
        bool SaveChangesContent(t_Content content);
        //remove content
        bool RemoveContent(t_Content content);
        bool RemoveContent(Guid guid);


        /**
         * Tag operations
         * */
        void AddTagsToContent(string[] tag);
        void EditTagToContent(string oldTag,string newTag);
        void RemoveTagsToContent(string[] tag);
        //criteria
        void AddCriteriaToContent(string[] tag);
        void EditCriteriaToContent(string oldTag, string newTag);
        void RemoveCriteriaToContent(string[] tag);

        //IQueryable<T> All<T>() where T:class;


        void CreateTestUpload();
    }
}
