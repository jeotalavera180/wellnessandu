﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Helper
    {
        /// <summary>
        /// check status
        /// </summary>
        /// <param name="includeDeleted"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static bool CheckStatus(bool includeDeleted, int? status)
        {
            if (status != null)
            {
                if (includeDeleted)
                {
                    return true;
                }
                else
                {
                    if (status == (int)Status.Deleted)
                    {
                        return false;
                    }
                    return true;
                }
            }

            return true;

        }
        /// <summary>
        /// Compare two strings if they are equal. trims them as well
        /// </summary>
        /// <param name="s1"></param>
        /// <param name="s2"></param>
        /// <returns></returns>
        public static bool TextMatch(string s1, string s2)
        {
            if (s1.Trim().ToLower() == s2.Trim().ToLower())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string StringArrayToString(string[] array, char separator = ',')
        {
            string builder = "";
            int i = 0;
            foreach (string a in array.OrderBy(d => d))
            {
                i++;
                builder += i != array.Count() ? (a.ToLower().Trim() + separator) : (a.ToLower().Trim());
            }
            return builder;
        }

    }
}
