﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
        public enum Status
        {
            Created = 0,
            Updated = 1,
            Deleted = 99,
        }
}
