﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Appsource_Wrapper_Lib;

namespace TestAPI.Controllers
{
    public class ValuesController : ApiController
    {
        public ValuesController()
        {
            SqlClientWrapper.Con = "DefaultConnection";
            SqlClientWrapper.UserName = "System";
        }
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
           // return SqlClientWrapper.getSet_JSON("hi", "hi");
            return SqlClientWrapper.getSet_JSON("select * from employees", "DefaultConnection", true);
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}