﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Appsource_Wrapper_Lib
{
    public class SqlClientWrapper : general
    {
        /// <summary>
        /// returns a json given an Sql Command, but you need to set .Con to your ConnectionString Name first.
        /// </summary>
        /// <param name="cmd">SqlCommand</param>
        /// <returns>json</returns>
        public static string getSet_JSON(SqlCommand cmd)
        {
            return DataSetToJSON(general.getSet(cmd));
        }

        /// <summary>
        /// Returns a Json given an sql statement and a client or connectionStringName defined on your webconfig
        /// </summary>
        /// <param name="sqlStatement">the sql statement</param>
        /// <param name="clientOrConnectionStringName">client or connectinstringname defined on your webconfig</param>
        /// <param name="isConnectionStringName">Specify if its a connectionstringname else its a client </param>
        /// <returns>Json</returns>
        public static string getSet_JSON(string sqlStatement, string clientOrConnectionStringName, bool isConnectionStringName = false)
        {
            return DataSetToJSON(general.getSet(sqlStatement, clientOrConnectionStringName, isConnectionStringName));
        }

        /// <summary>
        /// returns a json given an sql command and a client or connectionStringName defined on your webconfig
        /// </summary>
        /// <param name="cmd">SqlCommand</param>
        /// <param name="clientOrConnectionStringName">client or connectinstringname defined on your webconfig</param>
        /// <param name="isConnectionStringName">Specify if its a connectionstringname else its a client </param>
        /// <returns>json</returns>
        public static string getSet_JSON(SqlCommand cmd, string clientOrConnectionStringName, bool isConnectionStringName = false)
        {
            return DataSetToJSON(general.getSet(cmd, clientOrConnectionStringName, isConnectionStringName));
        }

        /// <summary>
        /// returns a json given an Sql statement but you need to set .Con to your ConnectionString Name first.
        /// </summary>
        /// <param name="sql">Sql Statement</param>
        /// <returns>JSON</returns>
        public static string getSet_JSON(string sql)
        {
            return DataSetToJSON(general.getSet(sql));
        }

        /// <summary>
        /// returns json set with input table and parameters.
        /// but you need to set .Con to your ConnectionString Name first.
        /// </summary>
        /// <param name="table">the name of the table you want to modify</param>
        /// <param name="parameters"> parameters</param>
        /// <returns>string</returns>
        public static string getSet_JSON(string table, params string[] parameters)
        {
            return "test";
            //return DataSetToJSON(general.getSet(cmd, client, isItClient));
        }

        /// <summary>
        /// DataSetToJSON
        /// </summary>
        /// <param name="DataSet">ds</param>
        /// <returns>bool</returns>
        static string DataSetToJSON(DataSet ds)
        {
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    return general.DStoJSON(ds);
                }
            }
            return "";
        }
    }
}
