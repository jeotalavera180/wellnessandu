﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Configuration;
using System.Net.Mail;
using System.Net;

/// <summary>
/// Summary description for general
/// </summary>
public class general
{
    //specify Connection String
    public static string Con;
    //Optional: specify username and location
    public static string UserName = "System";
    public static string UserLocation;

    /// <summary>
    /// returns a dataset given an Sql Command
    /// </summary>
    /// <param name="cmd">SqlCommand</param>
    /// <returns>Dataset</returns>
    public static DataSet getSet(SqlCommand cmd)
    {
        DataSet ds = new DataSet();
        using (SqlConnection con = SqlConn())
        {
            try
            {
                cmd.Connection = con;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds, "newset");
                return ds;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message.ToString());
                //add to log file
                new eException(ex);
                return ds;
            }
        }
    }

    /// <summary>
    /// Returns a dataset given an sql statement and a database
    /// </summary>
    /// <param name="sql">Sql Statement</param>
    /// <param name="client">Database</param>
    /// <returns>DataSet</returns>
    public static DataSet getSet(string sqlStatement, string client, bool isConnectionString = false)
    {
        DataSet ds = new DataSet();
        using (SqlConnection con = SqlConn(client, isConnectionString))
        {
            try
            {
                SqlCommand cmd = new SqlCommand(sqlStatement);
                cmd.Connection = con;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds, "newset");
                return ds;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message.ToString());
                //add to log file
                new eException(ex);
                return ds;
            }
        }
    }

    /// <summary>
    /// returns a dataset given an sql command and a database
    /// </summary>
    /// <param name="cmd">SqlCommand</param>
    /// <param name="client">client</param>
    /// <returns>Dataset</returns>
    public static DataSet getSet(SqlCommand cmd, string client, bool isConnectionString = false)
    {
        DataSet ds = new DataSet();
        using (SqlConnection con = SqlConn(client, isConnectionString))
        {
            try
            {
                cmd.Connection = con;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds, "newset");
                return ds;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message.ToString());
                //add to log file
                new eException(ex);
                return ds;
            }
        }

    }

    /// <summary>
    /// returns a dataset given an Sql statement
    /// </summary>
    /// <param name="sql">Sql Statement</param>
    /// <returns></returns>
    public static DataSet getSet(string sqlStatement)
    {
        DataSet ds = new DataSet();
        using (SqlConnection con = SqlConn())
        {
            try
            {
                SqlCommand cmd = new SqlCommand(sqlStatement);
                cmd.Connection = con;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds, "newset");
                return ds;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message.ToString());
                //add to log file
                new eException(ex);
                return ds;
            }
        }
    }

    /// <summary>
    /// Returns a boolean on an UPDATE/DELETE sql command using a Transaction
    /// Performs Single Row Update/Delete
    /// </summary>
    /// <param name="cmd">Sql Command</param>
    /// <returns>Yes/No</returns>
    public static bool performAction(SqlCommand cmd)
    {
        using (SqlConnection con = SqlConn())
        {
            using (SqlTransaction transaction = con.BeginTransaction())
            {
                try
                {
                    cmd.Connection = con;
                    cmd.Transaction = transaction;
                    int rowCount = cmd.ExecuteNonQuery();
                    if (rowCount == 1)
                    {
                        transaction.Commit();
                        return true;
                    }
                    else
                    {
                        transaction.Rollback();
                        System.Diagnostics.Debug.WriteLine("Affected Rows: " + rowCount);
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    System.Diagnostics.Debug.WriteLine(ex.Message.ToString());
                    //add to log file
                    new eException(ex);
                    return false;
                }
            }
        }
    }

    /// <summary>
    /// Performs an action UPDATE/DELETE given an sql command withOUT a transaction
    /// </summary>
    /// <param name="cmdd">Sql Command</param>
    /// <returns>Yes/No</returns>
    public static bool performActionNoTrans(SqlCommand cmdd)
    {
        using (SqlConnection con = SqlConn())
        {
            try
            {
                cmdd.Connection = con;
                int rowCount = cmdd.ExecuteNonQuery();
                if (rowCount > 0)
                    return true;
                else return false;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message.ToString());
                new eException(ex);
                return false;
            }
        }
    }

    /// <summary>
    /// Returns a boolean on an UPDATE/DELETE sql command using a transaction
    /// Performs Single Row Update/Delete
    /// </summary>
    /// <param name="cmd">Sql Command</param>
    /// <param name="client">Database</param>
    /// <returns>Yes/No</returns>
    public static bool performActionClient(SqlCommand cmd, string client)
    {
        using (SqlConnection con = SqlConn(client))
        {
            using (SqlTransaction transaction = con.BeginTransaction())
            {
                try
                {
                    cmd.Connection = con;
                    cmd.Transaction = transaction;
                    int rowCount = cmd.ExecuteNonQuery();
                    if (rowCount == 1)
                    {
                        transaction.Commit();
                        return true;
                    }
                    else
                    {
                        transaction.Rollback();
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    System.Diagnostics.Debug.WriteLine(ex.Message.ToString());
                    //add to log file
                    new eException(ex);
                    return false;
                }
            }
        }
    }

    /// <summary>
    /// Performs an action UPDATE/DELETE given an sql command withOUT a transaction
    /// </summary>
    /// <param name="cmdd">Sql Command</param>
    /// <param name="client">Client</param>
    /// <returns>Yes/No</returns>
    public static bool performActionNoTransClient(SqlCommand cmdd, string client)
    {
        using (SqlConnection con = SqlConn(client))
        {
            try
            {
                cmdd.Connection = con;
                int actiondone = cmdd.ExecuteNonQuery();
                if (actiondone > 0)
                    return true;
                else return false;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message.ToString());
                //add to log file
                new eException(ex);
                return false;
            }
        }
    }

    /// <summary>
    /// returns a string of data given an sql command
    /// </summary>
    /// <param name="cmd">sqlCommand</param>
    /// <returns>String data</returns>
    public static string getSingleData(SqlCommand cmd)
    {
        using (SqlConnection con = SqlConn())
        {
            try
            {
                cmd.Connection = con;
                Object mydata = cmd.ExecuteScalar();
                if (mydata != null)
                {
                    return mydata.ToString();
                }
                else
                    return "";
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message.ToString());
                //add to log file
                new eException(ex);
                return "";
            }
        }
    }
    public static string getSingleData(SqlCommand cmd, string client)
    {
        using (SqlConnection con = SqlConn(client))
        {
            try
            {
                cmd.Connection = con;
                Object mydata = cmd.ExecuteScalar();
                if (mydata != null)
                {
                    return mydata.ToString();
                }
                else
                    return "";
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message.ToString());
                //add to log file
                new eException(ex);
                return "";
            }
        }
    }
    public static string getSingleData(String sql)
    {
        using (SqlConnection con = SqlConn())
        {
            try
            {
                SqlCommand cmd = new SqlCommand(sql);
                cmd.Connection = con;
                Object mydata = cmd.ExecuteScalar();
                if (mydata != null)
                {
                    return mydata.ToString();
                }
                else
                    return "";
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message.ToString());
                //add to log file
                new eException(ex);
                return "";
            }
        }
    }

    /// <summary>
    /// returns an Identity Value given an sql statement and a client database
    /// Adds the Scope_Identity to the statement Automatically
    /// </summary>
    /// <param name="sql">Sql Statement</param>
    /// <param name="client">Client</param>
    /// <returns></returns>
    public static string getIdentity(string sql, string client)
    {
        sql = sql + "; Select Scope_Identity()";
        using (SqlConnection con = SqlConn(client))
        {
            try
            {
                SqlCommand cmd = new SqlCommand(sql, con);
                Object identitydb = cmd.ExecuteScalar();
                if (identitydb == null)
                    return "0";
                else return identitydb.ToString();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message.ToString());
                //add to log file
                new eException(ex);
                return "0";
            }
        }
    }

    /// <summary>
    /// returns an identity value given an sqlcommand and client
    /// </summary>
    /// <param name="cmd">SqlCommand Stored Procedure or Sql Statement included</param>
    /// <param name="client">Client DB</param>
    /// <returns>String representing Identity Value</returns>
    public static string getIdentity(SqlCommand cmd, string client)
    {
        using (SqlConnection con = SqlConn(client))
        {
            try
            {
                cmd.Connection = con;
                Object identitydb = cmd.ExecuteScalar();
                if (identitydb == null)
                    return "0";
                else return identitydb.ToString();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message.ToString());
                //add to log file
                new eException(ex);
                return "0";
            }
        }
    }

    /// <summary>
    /// returns an Identity Value given an sql statement
    /// Adds the Scope_Identity to the statement Automatically
    /// </summary>
    /// <param name="sql">Sql Statement</param>
    /// <returns></returns>
    public static string getIdentity(string sql)
    {
        sql = sql + "; Select Scope_Identity()";
        using (SqlConnection con = SqlConn())
        {
            try
            {
                SqlCommand cmd = new SqlCommand(sql, con);
                Object identitydb = cmd.ExecuteScalar();
                if (identitydb == null)
                    return "0";
                else return identitydb.ToString();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message.ToString());
                //add to log file
                new eException(ex);
                return "0";
            }
        }
    }

    /// <summary>
    /// returns an identity value given an sqlcommand
    /// </summary>
    /// <param name="cmd">SqlCommand Stored Procedure or Sql Statement included</param>
    /// <param name="client">Client DB</param>
    /// <returns>String representing Identity Value</returns>
    public static string getIdentity(SqlCommand cmd)
    {
        using (SqlConnection con = SqlConn())
        {
            try
            {
                cmd.Connection = con;
                Object identitydb = cmd.ExecuteScalar();
                if (identitydb == null)
                    return "0";
                else return identitydb.ToString();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message.ToString());
                //add to log file
                new eException(ex);
                return "0";
            }
        }
    }

    /// <summary>
    /// MD5 Hash PHP like encryptor
    /// </summary>
    /// <param name="input"></param>
    /// <returns>string with the MD5 Hash</returns>
    public static string getMd5Hash(string input)
    {
        MD5 md5Hasher = MD5.Create();
        byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));
        StringBuilder sBuilder = new StringBuilder();
        for (int i = 0; i < data.Length; i++)
        {
            sBuilder.Append(data[i].ToString("x2"));
        }
        return sBuilder.ToString();
    }

    /// <summary>
    /// CDS Get status for UI
    /// </summary>
    /// <param name="UI"></param>
    /// <returns></returns>
    public static String getStatus(string UI)
    {
        using (SqlConnection con = SqlConn())
        {
            SqlCommand cmd = new SqlCommand(@"SELECT refname FROM t_ref WHERE UI=@UI", con);
            cmd.Parameters.Add("@UI", SqlDbType.VarChar).Value = UI;
            return getSingleData(cmd);
        }
    }
    /// <summary>
    /// get user session variable
    /// </summary>
    /// <returns></returns>
    public static string getUser()
    {
        if (String.IsNullOrEmpty(UserName))
        {
            if (HttpContext.Current.Session["FullName"] != null)
                return HttpContext.Current.Session["FullName"].ToString();
            else
                HttpContext.Current.Response.Redirect("login.aspx");
            return "(no user in session)";
        }
        else
        {
            return UserName;
        }
    }
    /// <summary>
    /// get Userlocation variable from the session
    /// </summary>
    /// <returns></returns>
    public static string getUserLocation()
    {
        if (string.IsNullOrEmpty(UserLocation))
        {
            if (HttpContext.Current.Session["UserLocation"] != null)
                return HttpContext.Current.Session["UserLocation"].ToString();
            else
                HttpContext.Current.Response.Redirect("login.aspx");
            return "TESTING";
        }
        else return "TESTING";
    }

    /// <summary>
    /// Opens an Sql Connection given the connection string in the web.config under "AppConnection"
    /// </summary>
    /// <returns>Open Connection</returns>
    internal static SqlConnection SqlConn()
    {
        ConnectionStringSettingsCollection connections =
        ConfigurationManager.ConnectionStrings;

        string SqlConn = connections[Con].ConnectionString;
        SqlConnection con = new SqlConnection(SqlConn);
        try
        {
            con.Open();
        }
        catch (Exception ex)
        {
            System.Diagnostics.Debug.WriteLine(ex.Message.ToString());
            //add to log file
            new eException(ex);
        }
        return con;
    }
    /// <summary>
    /// Returns an open connection given the AppSetting value of "App_Clients"
    /// </summary>
    /// <param name="Client">Database</param>
    /// <returns>Open Connection</returns>
    internal static SqlConnection SqlConn(String Client)
    {
        String AppSettingCon = ConfigurationManager.AppSettings.Get("App_Clients");
        string SqlConn = String.Format(AppSettingCon, Client);
        SqlConnection con = new SqlConnection(SqlConn);
        try
        {
            con.Open();
        }
        catch (Exception ex)
        {
            System.Diagnostics.Debug.WriteLine(ex.Message.ToString());
            //add to log file
            new eException(ex);
        }
        return con;
    }

    /// <summary>
    /// Returns an open connection given the AppSetting value of "App_Clients"
    /// </summary>
    /// <param name="Client">Database</param>
    /// <returns>Open Connection</returns>
    internal static SqlConnection SqlConn(string clientOrConnectionStringName, bool isConnectionString)
    {
        //is Client
        if (isConnectionString == false)
        {
            String AppSettingCon = ConfigurationManager.AppSettings.Get("App_Clients");
            string SqlConn = String.Format(AppSettingCon, clientOrConnectionStringName);
            SqlConnection con = new SqlConnection(SqlConn);
            try
            {
                con.Open();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message.ToString());
                //add to log file
                new eException(ex);
            }
            return con;
        }
        //Connectionstring
        else
        {
            ConnectionStringSettingsCollection connections =
         ConfigurationManager.ConnectionStrings;

            string SqlConn = connections[clientOrConnectionStringName].ConnectionString;
            SqlConnection con = new SqlConnection(SqlConn);
            try
            {
                con.Open();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message.ToString());
                //add to log file
                new eException(ex);
            }
            return con;
        }
    }


    /// <summary>
    /// Sends an email using System NET mail
    /// </summary>
    /// <param name="emailTo">list of emails separated by Comma (,)</param>
    /// <param name="Subject">Subject of the email</param>
    /// <param name="Body">email body</param>
    /// <returns>true/false on success/failure</returns>
    public static bool sendMail(String emailTo, String Subject, String Body)
    {
        if (Convert.ToBoolean(ConfigurationManager.AppSettings.Get("emailActive")))
        {
            try
            {
                DataSet ds = general.getSet("SELECT * FROM t_settings");
                DataRow[] smtpHost = ds.Tables[0].Select("settingType = 'smtp'");
                DataRow[] smtpPort = ds.Tables[0].Select("settingType = 'smtpport'");
                DataRow[] smtpUsername = ds.Tables[0].Select("settingType = 'smtpusername'");
                DataRow[] smtpPassword = ds.Tables[0].Select("settingType = 'smtppassword'");
                DataRow[] smtpSSL = ds.Tables[0].Select("settingType = 'smtpSSL'");


                //SmtpClient smtpClient = new SmtpClient();
                //NetworkCredential credential = new NetworkCredential();
                //credential.Domain = smtpHost[0]["settingvalue"].ToString();
                //credential.UserName = smtpUsername[0]["settingvalue"].ToString();
                //credential.Password = smtpPassword[0]["settingvalue"].ToString();
                //smtpClient.Credentials = credential;
                ////smtpClient.UseDefaultCredentials = false;
                //using (MailMessage message = new MailMessage())
                //{
                //    smtpClient.Host = smtpHost[0]["settingvalue"].ToString();
                //    smtpClient.Port = Convert.ToInt32(smtpPort[0]["settingvalue"].ToString());
                //    smtpClient.EnableSsl = Convert.ToBoolean(smtpSSL[0]["settingvalue"].ToString());
                //    message.To.Add(emailTo);
                //    message.From = new MailAddress(smtpUsername[0]["settingvalue"].ToString());
                //    message.CC.Add(smtpUsername[0]["settingvalue"].ToString());
                //    message.Subject = Subject;
                //    message.Body = Body;
                //    message.Priority = MailPriority.High;
                //    message.CC.Add("aresh@equadtech.com");
                //    message.IsBodyHtml = true;

                //    smtpClient.Send(message);
                //    return true;
                //}

                MailMessage msg = new MailMessage(smtpUsername[0]["settingvalue"].ToString(), emailTo, Subject, Body);
                msg.IsBodyHtml = true;
                msg.CC.Add(smtpUsername[0]["settingvalue"].ToString());
                msg.CC.Add("ycthean@equadtech.com");
                System.Net.NetworkCredential cred = new
                System.Net.NetworkCredential(smtpUsername[0]["settingvalue"].ToString(), smtpPassword[0]["settingvalue"].ToString());
                System.Net.Mail.SmtpClient mailClient = new
                System.Net.Mail.SmtpClient(smtpHost[0]["settingvalue"].ToString(), Convert.ToInt32(smtpPort[0]["settingvalue"].ToString()));
                mailClient.EnableSsl = true;
                mailClient.UseDefaultCredentials = false;
                mailClient.Credentials = cred;
                mailClient.Send(msg);
                return true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message.ToString());
                //add to log file
                new eException(ex);
                return false;
            }
        }
        else return false;

    }


    public static string GetRandomPasswordUsingGUID(int length)
    {
        // Get the GUID
        string guidResult = System.Guid.NewGuid().ToString();

        // Remove the hyphens
        guidResult = guidResult.Replace("-", string.Empty);

        // Make sure length is valid
        if (length <= 0 || length > guidResult.Length)
            new ArgumentException("Length must be between 1 and " + guidResult.Length);

        // Return the first length bytes
        return guidResult.Substring(0, length);
    }
    public static string DStoJSON(DataSet ds)
    {
        StringBuilder json = new StringBuilder();

        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            json.Append("{");

            int i = 0;
            int colcount = dr.Table.Columns.Count;

            foreach (DataColumn dc in dr.Table.Columns)
            {
                json.Append("\"");
                json.Append(dc.ColumnName);
                json.Append("\":\"");
                json.Append(dr[dc]);
                json.Append("\"");

                i++;
                if (i < colcount) json.Append(",");

            }
            json.Append("}");
            json.Append(",");
        }
        json = json.Remove(json.Length - 1, 1);
        return json.ToString();
    }
    public static string RelativeDate(DateTime theDate)
    {
        Dictionary<long, string> thresholds = new Dictionary<long, string>();
        int minute = 60;
        int hour = 60 * minute;
        int day = 24 * hour;
        thresholds.Add(60, "{0} seconds ago");
        thresholds.Add(minute * 2, "a minute ago");
        thresholds.Add(45 * minute, "{0} minutes ago");
        thresholds.Add(120 * minute, "an hour ago");
        thresholds.Add(day, "{0} hours ago");
        thresholds.Add(day * 2, "yesterday");
        thresholds.Add(day * 30, "{0} days ago");
        thresholds.Add(day * 365, "{0} months ago");
        thresholds.Add(long.MaxValue, "{0} years ago");

        long since = (DateTime.Now.Ticks - theDate.Ticks) / 10000000;
        foreach (long threshold in thresholds.Keys)
        {
            if (since < threshold)
            {
                TimeSpan t = new TimeSpan((DateTime.Now.Ticks - theDate.Ticks));
                return string.Format(thresholds[threshold], (t.Days > 365 ? t.Days / 365 : (t.Days > 0 ? t.Days : (t.Hours > 0 ? t.Hours : (t.Minutes > 0 ? t.Minutes : (t.Seconds > 0 ? t.Seconds : 0))))).ToString());
            }
        }
        return "";
    }
}